package net.codejava;
	
class Point {
		
	private int x;
	private int y;
		
	Point(int x, int y){
		this.x = x;
		this.y = y;
	}
		
	int getX(){
		return x;
	}
		
	int getY() {
		return y;
	}
		
	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
		
	void draw() {
		System.out.println("Point drawn at " + toString());
	}

}

class Circle extends Point {
	private int radius;
	
	Circle(int x, int y, int radius){
		super(x, y);
		this.radius = radius;
	}
	
	int getRadius() {
		return radius;
	}
	
	@Override
	public String toString() {
		return "" + radius;
	}
	
	void draw() {
		System.out.println("Circle is drawn at " + super.toString() + " " + toString());
	}
}
	
public class Session4Graphics {
	public static void main(String[] args) {
		Point p = new Point(10, 20);
		Circle c = new Circle(10, 20, 30);
		p.draw();
		c.draw();
	}
}
