package net.codejava;

public class Session3BMICalculator {
	
	public static void main(String[] args) {
		System.out.println("Underweight: Less than 18.5");
		System.out.println("Normal weight: 18.5- 24.9");
		System.out.println("Overweight: 25-29.9");
		System.out.println("Obesity: 30 or higher");
		Person p = new Person(66, 125);
		System.out.println("Your BMI: " + p.calculateBMI());
	}

}

class Person{

	private int personHeight;
	private int personWeight;
	
	public Person(int height, int weight) {
		personHeight = height;
		personWeight = weight;
	}
	
	public int calculateBMI() {
		int BMI = (personWeight * 703)/(personHeight * personHeight);
		return BMI;
	}
	
}
