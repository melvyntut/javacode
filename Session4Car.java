package net.codejava;

public class Session4Car {
	
	private String make;
	private String model;
	private int year;
	private Session4Engine engine;
	
	Session4Car(String make, String model, int year, Session4Engine engine){
		this.make = make;
		this.model = model;
		this.year = year;
		this.engine = engine;
	}
	
	String getMake() {
		return this.make;
	}
	
	String getModel() {
		return this.model;
	}
	
	int getYear() {
		return this.year;
	}
	
	Session4Engine getEngine() {
		return this.engine;
	}

}
