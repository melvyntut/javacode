package net.codejava;

public class Session2StringToInt {
	
	public static void main(String[] args) {
		
		String s = "200";
		int i = Integer.parseInt(s);
		System.out.println(i);
		long l = i;
		System.out.println(l);
		double d = l;
		System.out.println(d);
		float f = (float)d;
		System.out.println(f);
		
	}

}
