package net.codejava;

public class Session3Employee {
	
	public static void main(String[] args) {
		
		Worker w = new Worker("Melvyn", 21);
		w.print();
			
	}
}

class Worker{
	
	private String workerName;
	private int workerAge;
	private String workerJob;
	private int workerSalary;
	
	public Worker(String name, int age) {
		workerName = name;
		workerAge = age;
	}
	
	public void print() {
		System.out.println("Name: " + workerName);
		System.out.println("Age: " + workerAge);
	}
	
}
	

