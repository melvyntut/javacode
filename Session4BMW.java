package net.codejava;

public class Session4BMW extends Session4Car {
	
	private int numWheels;
	
	Session4BMW(String make, String model, int year, Session4Engine engine, int numWheels){
		super(make, model, year, engine);
		this.numWheels = numWheels;
	}
	
	public static void main(String[] args) {
		Session4BMW bmw = new Session4BMW("make", "model", 2020, new Session4Engine(), 4);
	}

}
