package net.codejava;

public class Session4Task1 {
	
	/*
	 	1) Single inheritance is when a class can inherit from one class.
	 	Multiple inheritance is when a class can inherit from two or more
	 	classes.
	 	2) Is-A inheritance means the class is a type of something e.g. a
	 	car is a type of vehicle. Has-A inheritance means the class has 
	 	something e.g. a car has an engine.
	 	3) Using interfaces makes the code easier to organise and adapt,
	 	which is useful for agile development's flexibility
	 	4) Using inheritance allows you to cut down on duplicate code
	 	because it takes existing code from another class. This also 
	 	prevents errors because it reuses code that already works.
	 	5) a) True b) False c) False d) False
	 
	 
	 */

}
