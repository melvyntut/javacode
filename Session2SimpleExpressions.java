package net.codejava;

public class Session2SimpleExpressions {
	
	public static void main(String[] args)
	   {
	      int counter = 10;
	      double temperature = 98.6; 
	      String firstName = "David";
	      int[] ages = { 52, 28, 93, 16 };
	      char gradeLetters[] = { 'A', 'B', 'C', 'D', 'F' };
	      float[][] matrix = { { 1.0F, 2.0F, 3.0F }, { 4.0F, 5.0F, 6.0F }};
	      int x = 1, y[] = { 1, 2, 3 }, z = 3;
	      double π = 3.14159;
	      
	      System.out.println(counter);
	      System.out.println(temperature);
	      System.out.println(firstName);
	      System.out.println(ages);
	      System.out.println(gradeLetters);
	      System.out.println(matrix);
	      System.out.println(x);
	      System.out.println(y);
	      System.out.println(z);
	      System.out.println(π);
	      
	   }


}
