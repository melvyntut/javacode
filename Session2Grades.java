package net.codejava;
import java.util.Scanner;

public class Session2Grades {
	
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		int exceptional = 95;
		int outstanding = 85;
		int first = 75;
		int twoOne = 62;
		int twoTwo = 52;
		int three = 42;
		int fail = 32;
		int abjectFail = 0;
		
		while(true) {
		
			int grade = sc.nextInt();
		
			if(grade >= exceptional) {
				System.out.println("Exceptional 1st");
			} else if(grade >= outstanding && grade < exceptional) {
				System.out.println("Outstanding 1st");
			} else if(grade >= first && grade < outstanding) {
				System.out.println("First");
			} else if(grade >= twoOne && grade < first) {
				System.out.println("2:1");
			} else if(grade >= twoTwo && grade < twoOne) {
				System.out.println("2:2");
			} else if(grade >= three && grade < twoTwo) {
				System.out.println("3");
			} else if(grade >= fail && grade < three) {
				System.out.println("Fail");
			} else if(grade >= abjectFail && grade < fail) {
				System.out.println("Abject Fail");
			}
		
		}
		
	}

}
